//
//  PlayGameScene.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 19/08/2016.
//  Copyright © 2016 Dixon, Matthew Stanley. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit
import AVFoundation
import Social

class PlayGameScene: SKScene, SKPhysicsContactDelegate {
    
    
    var movingGround : MovingGround!
    var movingBackGround : MovingBackGround!
    var movingMidGround : MovingMidGround!
    var movingForeGround : MovingForeGround!
    var birdy : Birdy!
    var dumby : Dumby!
    
    
    let MovingForeGroundTexture = SKTexture(imageNamed: "MovingForeGround")
    let MovingMidGroundTexture = SKTexture(imageNamed: "MovingMidGround")
    let MovingBackGroundTexture = SKTexture(imageNamed: "MovingBackGround")
    let MovingGroundTexture = SKTexture(imageNamed: "MovingGround")
    let Pipe1Texture = SKTexture(imageNamed: "Pipe1Facts")
    let Pipe2Texture = SKTexture(imageNamed: "Pipe2Facts")
    let MovingBirdyTexture = SKTexture(imageNamed: "Birdy1")
    let DumbyTexture = SKTexture(imageNamed: "Birdy1")
    
    
    var skyColour = UIColor(red: 133.0/255.0, green: 197.0/255.0, blue: 207.0/255/0, alpha: 1.0)
    var moving = SKNode()
    var PipePear = SKNode()
    var Pipes = SKNode()
    var groundLevel = SKNode()
    var skyLimit = SKNode()
    
    var alreadyAddedToTheScene = Bool()
    
    var movePipesAndRemove = SKAction()
    var SpawnThenDelayForEver = SKAction()
    
    let BirdyCategory: UInt32 = 1 << 0
    let worldCategory: UInt32 = 1 << 1
    let pipeCategory: UInt32 = 1 << 2
    
    let scoreCategory: UInt32 = 1 << 3
    
    var score = NSInteger()
    var scorelabelNode = SKLabelNode()
    
    var gameSceneAudioPlayer = AVAudioPlayer()
    var gameSceneSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("GameMusic", ofType: "mp3")!)
    var gameSoundAudioEffect = AVAudioPlayer()
    var gameSoundEffect = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("CourseDemoGameOverEffect", ofType: "mp3")!)
    
    var userSettingDefults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        // add moving node to the scene
        addChild(moving)
        
        // gravity properties
        self.physicsWorld.gravity = CGVectorMake(0.0, -5.5)
        self.physicsWorld.contactDelegate = self
        
        
        // bool used to add stuff to the scene
        alreadyAddedToTheScene = false
        
        
        //setting the background colour
        backgroundColor = skyColour
        
        // call an instance of MovingGround class
        
        movingGround = MovingGround(size: CGSizeMake(MovingGroundTexture.size().width, MovingGroundTexture.size().height))
        moving.addChild(movingGround)
        
        //call an instance of the MovingBackGround Class
        movingBackGround = MovingBackGround(size: CGSizeMake(MovingBackGroundTexture.size().width, MovingBackGroundTexture.size().height))
        movingBackGround.zPosition = -3
        moving.addChild(movingBackGround)
        
        //call an instance of the MovingMidGround Class
        
        movingMidGround = MovingMidGround(size: CGSizeMake(MovingMidGroundTexture.size().width, MovingMidGroundTexture.size().height))
        movingMidGround.zPosition = -2
        moving.addChild(movingMidGround)
        
        // call an instance of the MOvingMidGround class
        movingForeGround = MovingForeGround(size: CGSizeMake(MovingForeGroundTexture.size().width, MovingForeGroundTexture.size().height))
        movingForeGround.zPosition = -1
        moving.addChild(movingForeGround)
        
        // add the pipes node to the moving node as a child
        moving.addChild(Pipes)
        
        // adding and removing pipe1 and pipe2 too and from the game scene and it will also set the speed that it moves across the scene
        let distanceToMove = CGFloat(self.frame.size.width + 5.0 * Pipe1Texture.size().width)
        let movePipes = SKAction.moveByX(-distanceToMove, y: 0.0, duration: NSTimeInterval(0.004 * distanceToMove))
        let removePipes = SKAction.removeFromParent()
        movePipesAndRemove = SKAction.sequence([movePipes, removePipes])
        
        // attach the spawn pipes func directly inot the game scene
        let spawn = SKAction.runBlock({() in self.spawnPipes()})
        
        // delays between each spawn pipes func
        let delay = SKAction.waitForDuration(1.9, withRange: 2.0)
        let spawnThenDelay = SKAction.sequence([spawn, delay])
        SpawnThenDelayForEver = SKAction.repeatActionForever(spawnThenDelay)
        
        //call an instance of the dumby texture class
        dumby = Dumby(size: CGSizeMake(DumbyTexture.size().width, DumbyTexture.size().height))
        dumby.position = CGPoint(x: self.frame.size.width / 2.8, y: self.frame.size.height / 2)
        self.addChild(dumby)
        
        // call an instance of the Birdy Node
        birdy = Birdy(size: CGSizeMake(MovingBirdyTexture.size().width, MovingBirdyTexture.size().height))
        birdy.position = CGPoint(x: self.frame.size.width / 2.8, y: self.frame.size.height / 2)
        
        //Sk physics body proterties
        birdy.physicsBody = SKPhysicsBody(circleOfRadius: birdy.size.height / 2)
        birdy.physicsBody?.dynamic = true
        birdy.physicsBody?.allowsRotation = false
        //add birdy to it's own category
        birdy.physicsBody?.categoryBitMask = BirdyCategory
        //birdy can hit the world and the pipes category
        birdy.physicsBody?.collisionBitMask = worldCategory | pipeCategory
        //notication is made when the bird hits the world or the pipes
        birdy.physicsBody?.contactTestBitMask = worldCategory | pipeCategory
        
        
        // create a floor so the birdy cannot fall through the game scene
        groundLevel.position = CGPointMake(self.frame.width / 2, MovingGroundTexture.size().height / 2)
        //Sk physics body properties
        groundLevel.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width, MovingGroundTexture.size().height))
        groundLevel.physicsBody?.dynamic = false
        // add the ground to the world category
        groundLevel.physicsBody?.categoryBitMask = worldCategory
        //notication is made when birdy hits the ground
        groundLevel.physicsBody?.contactTestBitMask = BirdyCategory
        self.addChild(groundLevel)
        
        
        //create a boundary around the edge of the screen
        skyLimit.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        skyLimit.physicsBody?.friction = 0
        //add the sky to the world category
        skyLimit.physicsBody?.categoryBitMask = worldCategory
        //notication is made when birdy hits the ground
        skyLimit.physicsBody?.contactTestBitMask = BirdyCategory
        self.addChild(skyLimit)
        
        //setting up the score label
        score = 0
        scorelabelNode.fontName = "Halvetica"
        scorelabelNode.position = CGPointMake(self.frame.size.width / 1.2, self.frame.size.height / 1.2)
        scorelabelNode.fontColor = UIColor.darkGrayColor()
        scorelabelNode.zPosition = 1
        scorelabelNode.text = "\(score)"
        scorelabelNode.fontSize = 65
        self.addChild(scorelabelNode)
        
        //cheching the mute state in NS user defults
        //checkMuteState()
        //retrive the mute state
        //getBackMuteState()
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        addStuffToTheScene()
        
        alreadyAddedToTheScene = true
        
        if(moving.speed > 0) {
            
            birdy.physicsBody?.velocity = CGVectorMake(0, 0)
            birdy.physicsBody?.applyImpulse(CGVectorMake(0, 31))
            
        }
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        
        if (moving.speed > 0) {
            //tilts the birdy up and down
            birdy.zRotation = self.tiltConstants(-1, max: 0.5, value: birdy.physicsBody!.velocity.dy * (birdy.physicsBody!.velocity.dx < 0 ? 0.003: 0.001))
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact){
        
        if (moving.speed > 0) {
            //dect to see if the scene is moving as collision detection should only work when the scene is moving
            
            if ((contact.bodyA.categoryBitMask & scoreCategory) == scoreCategory || (contact.bodyB.categoryBitMask & scoreCategory) == scoreCategory) {
                
                //increments the score
                score++
                
                //saving the score to be used in the game over scene
                kScore = score
                
                //update score label node in the scene to display current score
                scorelabelNode.text = "\(score)"
                
                
            } else {
                //stop the scene from moving
                moving.speed = 0
                //collision detection with the world category
                birdy.physicsBody?.collisionBitMask = worldCategory
                //an attempt to bring the birdy to the ground have a collision has been detected
                
                let rotateBirdy = SKAction.rotateByAngle(0.01, duration: 0.003)
                let stopBirdy = SKAction.runBlock({()in self.StopBirdy()})
                let slowDownSequence = SKAction.sequence([rotateBirdy, stopBirdy])
                birdy.runAction(slowDownSequence)
                //stop music when collision is detected
                gameSceneAudioPlayer.stop()
                //play game over sound effect
                playGameOverEffectAudio()
                
                //take screen shot and save to a varible
                if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
                    
                    delay(0.2){
                        //cal the func that take the screen shot
                        self.gameEndScreenShot()
                    }
                    
                    
                }
                
                
                delay(0.5) {
                    self.gameOver()
                }
            }
        }
    }
    
    func addStuffToTheScene() {
        if (alreadyAddedToTheScene) == false {
            //call everything you want to be added to the scene
            
            movingGround.Begin()
            movingBackGround.Begin()
            movingMidGround.Begin()
            movingForeGround.Begin()
            
            self.runAction(SpawnThenDelayForEver)
            
            dumby.removeFromParent()
            addChild(birdy)
            birdy.Begin()
            
            playGameSceneAudio()
            
        }
    }
    
    
    
    
    func spawnPipes() {
        
        //a node to add the two pipe nodes too
        

        
        
        let pipePear = SKNode()
        pipePear.position = CGPointMake(self.frame.size.width + Pipe1Texture.size().width, 0)
        pipePear.zPosition = 0
        
        // Pipes variable starting Y position
        
        let height = UInt(self.frame.height / 3)
        let y = UInt(arc4random()) % height
        
        
        // create a pipe node and add it to the pipe pear node
        
        let pipe1 = SKSpriteNode(texture: Pipe1Texture)
        pipe1.position = CGPointMake(0.0, CGFloat(y))
        // SKPhysics body properties
        pipe1.physicsBody = SKPhysicsBody(rectangleOfSize: pipe1.size)
        pipe1.physicsBody?.dynamic = false
        //adds the pipe to the world category for collision detection
        pipe1.physicsBody?.categoryBitMask = pipeCategory
        // notication made when the bird hits the pipe
        pipe1.physicsBody?.contactTestBitMask = BirdyCategory
        pipePear.addChild(pipe1)
        
        //pipes varibale gap
        
        let maxGap = UInt(self.frame.height / 5)
        let minGap = UInt32(self.frame.height / 8)
        let Gap = UInt(arc4random_uniform(minGap)) + maxGap
        
        // create pipe 2 and set the position with the verical height gap and add it to the pipe pear node
        
        let pipe2 = SKSpriteNode(texture: Pipe2Texture)
        pipe2.position = CGPointMake(0.0, CGFloat(y) + pipe2.size.height + CGFloat(Gap))
        // SKPhysics body properties
        pipe2.physicsBody = SKPhysicsBody(rectangleOfSize: pipe2.size)
        pipe2.physicsBody?.dynamic = false
        //adds the pipe to the world category for collision detection
        pipe2.physicsBody?.categoryBitMask = pipeCategory
        // notication made when the bird hits the pipe
        pipe2.physicsBody?.contactTestBitMask = BirdyCategory
        
        //counting/decting the score using collision dectection on pipe 1
        let contactBirdyNode = SKNode()
        contactBirdyNode.position = CGPointMake(pipe1.size.width + birdy.size.width, CGRectGetMidY(self.frame))
        contactBirdyNode.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(pipe1.size.width, self.frame.size.height))
        contactBirdyNode.physicsBody?.dynamic = false
        contactBirdyNode.physicsBody?.categoryBitMask = scoreCategory
        contactBirdyNode.physicsBody?.contactTestBitMask = BirdyCategory
        pipePear.addChild(contactBirdyNode)
        
        
        
        pipePear.addChild(pipe2)
        pipePear.runAction(movePipesAndRemove)
        
        Pipes.addChild(pipePear)
        
    }
    
    //birdy tilt constrants
    
    func tiltConstants (min: CGFloat, max: CGFloat, value: CGFloat) -> CGFloat {
        if (value > max ) {
            return max
        } else if (value < min) {
            return min
        }else{
            return value
        }
    }
    
    func StopBirdy() {
        //birdy speed turns to 0 when it hits something.
        
        birdy.speed = 0
        
    }
    
    func delay(delay: Double, closure: ()->()) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))),dispatch_get_main_queue(), closure)
    }
    
    func playGameSceneAudio(){
        
        //setting up the audio for the game scene
        do {
            try gameSceneAudioPlayer = AVAudioPlayer(contentsOfURL: gameSceneSound)
        } catch {
            print("GameScene. gameSceneAudioPlayer is not available")
        }
        gameSceneAudioPlayer.prepareToPlay()
        gameSceneAudioPlayer.numberOfLoops = -1
        
        //chech to see if muted
        if kMuteState == true {
            if (gameSceneAudioPlayer.playing) {
                gameSceneAudioPlayer.stop()
            }
        }else if kMuteState == false {
            gameSceneAudioPlayer.play()
        }
    }
    
    func playGameOverEffectAudio() {
        //setting up the audio for the game over effect
        
        do {
            try gameSoundAudioEffect = AVAudioPlayer(contentsOfURL: gameSoundEffect)
        } catch {
            print("GameScene. gameOverAudio is not available")
        }
        gameSoundAudioEffect.prepareToPlay()
        
        //chech to see if muted
        if kMuteState == true {
            if (gameSoundAudioEffect.playing) {
                gameSoundAudioEffect.stop()
            }
        }else if kMuteState == false{
            //sound will be muted
            gameSoundAudioEffect.play()
        }
        
    }
    
    /*func checkMuteState(){
    //chech to see if we have already have key/object saved
    if (userSettingDefults.objectForKey("MuteState") != nil) {
    //we found a match. do nothing
    }else{
    // did not find a match
    userSettingDefults.setBool(false, forKey: "MuteState")
    userSettingDefults.synchronize()
    }
    }
    
    func getBackMuteState() {
    //retive the mute state
    kMuteState = userSettingDefults.boolForKey("MuteState")
    }
    */
    func gameOver() {
        
        //create the new scene to transition to
        let SkView = self.view! as SKView
        SkView.ignoresSiblingOrder = true
        // remove scene before transion
        self.scene?.removeFromParent()
        //the transion effect
        let transion = SKTransition.fadeWithColor(UIColor.grayColor(), duration: 1.0)
        transion.pausesOutgoingScene = false
        //a varible to hold the gameOver scene
        var scene: GameOver!
        scene = GameOver(size: SkView.bounds.size)
        //setting the new scene's aspect ration to fit
        scene.scaleMode = .AspectFill
        //presenting the new scene with a transition affect
        SkView.presentScene(scene, transition: transion)
        
    }
    
    
    func gameEndScreenShot(){
        // take a screen shot using the extention
        
        let takeScreenShot = self.view?.takeScreenShot()
        
        //save the screen shot to a constant variable
        kScreenShot = takeScreenShot!
        
    }
    
    
    
    

    
    
}
