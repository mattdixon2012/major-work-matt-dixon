//
//  MovingBackGround.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright © 2016 Dixon, Matthew Stanley. All rights reserved.
//

import Foundation
import SpriteKit

class MovingBackGround : SKSpriteNode {
    
    let MovingBackGroundTexture = SKTexture(imageNamed: "MovingBackGround")
    let MovingGroundTexture = SKTexture(imageNamed: "MovingGround")
    
    init(size: CGSize) {
        super.init(texture: nil, color: UIColor.clearColor(), size: CGSizeMake(size.width, size.height))
        anchorPoint = CGPointMake(0, 0)
        position = CGPointMake(0.0, 0.0)
        
        // looping the background montain texture from left to right
        
        for var i:CGFloat = 0; i<2 + self.frame.size.width / (MovingBackGroundTexture.size().width); ++i {
            let groundsprite = SKSpriteNode(texture: self.MovingBackGroundTexture)
            groundsprite.zPosition = 0
            groundsprite.anchorPoint = CGPointMake(0, 0)
            groundsprite.position = CGPointMake(i * groundsprite.size.width, 0)
            self.addChild(groundsprite)
        }

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func Begin() {
        //moving the background at speed
        
        let moveGroundSprite = SKAction.moveByX(-MovingBackGroundTexture.size().width, y: 0, duration: NSTimeInterval(0.3 * MovingBackGroundTexture.size().width))
        let resetGroundSprite = SKAction.moveByX(MovingBackGroundTexture.size().width, y: 0, duration: 0.0)
        let moveSequence = SKAction.sequence([moveGroundSprite, resetGroundSprite])
        
        runAction(SKAction.repeatActionForever(moveSequence))
        
    }

    
    
}