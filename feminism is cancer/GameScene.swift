//
//  GameScene.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright (c) 2016 Dixon, Matthew Stanley. All rights reserved.
//

import SpriteKit
import UIKit
import AVFoundation
import Social

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var bgColour = UIColor(red: 232.0/255.0, green: 78.0/255.0, blue: 132.0/255.0, alpha: 1.0)
    
    var playAgainButton = UIButton()
    var playAgainButtonImage = UIImage(named: "PlayAgainButton") as UIImage!
    
    func delay(delay: Double, closure: ()->()) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))),dispatch_get_main_queue(), closure)
    }
    
    func playAgain() {
    
    //removes the play agin button from view
    playAgainButton.removeFromSuperview()
    
    //create the new scene to transition to
    let SkView = self.view! as SKView
    SkView.ignoresSiblingOrder = true
    // remove scene before transion
    self.scene?.removeFromParent()
    //the transion effect
    let transion = SKTransition.fadeWithColor(UIColor.grayColor(), duration: 1.0)
    transion.pausesOutgoingScene = false
    //a varible to hold the gameOver scene
    var scene: PlayGameScene!
    scene = PlayGameScene(size: SkView.bounds.size)
    //setting the new scene's aspect ration to fit
    scene.scaleMode = .AspectFill
    //presenting the new scene with a transition affect
    SkView.presentScene(scene, transition: transion)
    
    }
    
    
    
    func playAgainButtonAction(sender: UIButton) {
        
        delay(0.5) {
            //play the game again
            self.playAgain()
        }
    }
    
    
    override func didMoveToView(view: SKView) {
        
        backgroundColor = bgColour

        //create the play agin button
        self.playAgainButton = UIButton(type: UIButtonType.Custom)
        self.playAgainButton.setImage(playAgainButtonImage, forState: .Normal)
        self.playAgainButton.frame = CGRectMake(self.frame.size.width / 2, self.frame.size.height / 2, 180, 180)
        self.playAgainButton.layer.anchorPoint = CGPointMake(1.0, 1.0)
        self.playAgainButton.layer.zPosition = 0
        //make the play again button perfrom an action when it is touched
        self.playAgainButton.addTarget(self, action: "playAgainButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        delay(0.5) {
            view.addSubview(self.playAgainButton)
        }
    }
}
        
        
        