//
//  GameViewController.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright (c) 2016 Dixon, Matthew Stanley. All rights reserved.
//

import UIKit
import SpriteKit
import Social

extension UIView {
    //extending the UIView to take a screenshot
    
    func takeScreenShot() -> UIImage {
        
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, 1.0)
        
        drawViewHierarchyInRect(self.bounds, afterScreenUpdates: false)
        
        CGContextSetInterpolationQuality(UIGraphicsGetCurrentContext(), CGInterpolationQuality.Low)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return image
    }
}

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            skView.showsPhysics = false
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            scene.size = skView.bounds.size
            
            skView.presentScene(scene)
        }
        
        // advise NSNotifcation centre of view controller launch
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "FaceBookSheet:", name: "FaceBookID", object: nil)
        
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    func FaceBookSheet(notifcation: NSNotification) {
        
        let fBSheet = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        fBSheet.completionHandler = {
            
            result in
            switch result {
            case SLComposeViewControllerResult.Cancelled:
                //add some code to deal with the cancelation
                break
            case SLComposeViewControllerResult.Done:
                //add some to deal  with the post being completed
                break
                
            }
        }
        
        //content of the FaceBook post
        
        fBSheet.setInitialText("Enter some text")
        fBSheet.addImage(kScreenShot)
        
        self.presentViewController(fBSheet, animated: false, completion: {
            //optional cometion code
            
        })
    }
    
   
    
    
    
}
