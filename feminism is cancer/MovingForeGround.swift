//
//  MovingForeGround.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright © 2016 Dixon, Matthew Stanley. All rights reserved.
//

import Foundation
import SpriteKit

class MovingForeGround : SKSpriteNode {
    
    let MovingForeGroundTexture = SKTexture(imageNamed: "MovingForeGround")
    let MovingGroundTexture = SKTexture(imageNamed: "movingGround")
    
    init(size: CGSize){
        super.init(texture: nil, color: UIColor.clearColor(), size: CGSizeMake(size.width, size.height))
        anchorPoint = CGPointMake(0, 0)
        position = CGPointMake(0.0, 0.0)
        
        // looping the fore ground texture
        
        
        for var i:CGFloat = 0; i<2 + self.frame.size.width / (MovingForeGroundTexture.size().width); ++i {
            let groundsprite = SKSpriteNode(texture: self.MovingForeGroundTexture)
            groundsprite.zPosition = 0
            groundsprite.anchorPoint = CGPointMake(0, 0)
            groundsprite.position = CGPointMake(i * groundsprite.size.width, 0)
            self.addChild(groundsprite)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func Begin() {
        //moving the ground at speed
        
        let moveGroundSprite = SKAction.moveByX(-MovingForeGroundTexture.size().width, y: 0, duration: NSTimeInterval(0.05 * MovingForeGroundTexture.size().width))
        let resetGroundSprite = SKAction.moveByX(MovingForeGroundTexture.size().width, y: 0, duration: 0.0)
        let moveSequence = SKAction.sequence([moveGroundSprite, resetGroundSprite])
        
        runAction(SKAction.repeatActionForever(moveSequence))
        
    }
    
}
