//
//  MovingMidGround.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright © 2016 Dixon, Matthew Stanley. All rights reserved.
//

import Foundation
import SpriteKit

class MovingMidGround : SKSpriteNode {
    
    let MovingMidGroundTexture = SKTexture(imageNamed: "MovingMidGround")
    let MovingGroundTexture = SKTexture(imageNamed: "MovingGround")
    
    init(size: CGSize){
        super.init(texture: nil, color: UIColor.clearColor(), size: CGSizeMake(MovingMidGroundTexture.size().width,MovingMidGroundTexture.size().height))
        anchorPoint = CGPointMake(0, 0)
        position = CGPointMake(0.0, 0.0)
        
        // looping the mid ground texture
        
        
        for var i:CGFloat = 0; i<2 + self.frame.size.width / (MovingMidGroundTexture.size().width); ++i {
            let groundsprite = SKSpriteNode(texture: self.MovingMidGroundTexture)
            groundsprite.zPosition = 0
            groundsprite.anchorPoint = CGPointMake(0, 0)
            groundsprite.position = CGPointMake(i * groundsprite.size.width, 0)

            self.addChild(groundsprite)
        }

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func Begin() {
        //moving the ground at speed
        
        let moveGroundSprite = SKAction.moveByX(-MovingMidGroundTexture.size().width, y: 0, duration: NSTimeInterval(0.1 * MovingMidGroundTexture.size().width))
        let resetGroundSprite = SKAction.moveByX(MovingMidGroundTexture.size().width, y: 0, duration: 0.0)
        let moveSequence = SKAction.sequence([moveGroundSprite, resetGroundSprite])
        
        runAction(SKAction.repeatActionForever(moveSequence))
        
    }
    
}
