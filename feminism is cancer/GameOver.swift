//
//  GameOver.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright © 2016 Dixon, Matthew Stanley. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import AVFoundation

class GameOver: SKScene {
    
    var bgColour = UIColor(red: 232.0/255.0, green: 78.0/255.0, blue: 132.0/255.0, alpha: 1.0)
    var textGreenColour = UIColor(red: 232.0/255.0, green: 78.0/255.0, blue: 132.0/255.0, alpha: 1.0)

    
    var playAgainButton = UIButton()
    var playAgainButtonImage = UIImage(named: "PlayAgainButton") as UIImage!
    
    var scoreLabel = UILabel()
    var scoreLabelImage = UIImage(named: "ScoreBG") as UIImage!
    var scoreLabelImageView = UIImageView()
    
    
    var gameOverAudio = AVAudioPlayer()
    var gameOverSoundEffect = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("CourseDemoGameOver", ofType: "mp3")!)
    
    var muteButton = UIButton()
    var audioMutedButtomImage = UIImage(named: "AudioMutedButton") as UIImage!
    var audioUnmutedImage = UIImage(named: "AudioUnMutedButton") as UIImage!
    
    var userSettingDefults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    var fbButton = UIButton()
    var fbImage = UIImage(named: "FBButton")

    
    
    override func didMoveToView(view: SKView) {
        
        backgroundColor = bgColour
        // setting the background colour to a custom UIcolour
        
        //create the play agin button
        self.playAgainButton = UIButton(type: UIButtonType.Custom)
        self.playAgainButton.setImage(playAgainButtonImage, forState: .Normal)
        self.playAgainButton.frame = CGRectMake(self.frame.size.width / 2, self.frame.size.height / 1.75 , 180, 180)
        self.playAgainButton.layer.anchorPoint = CGPointMake(1.0, 1.0)
        self.playAgainButton.layer.zPosition = 0
        //make the play again button perfrom an action when it is touched
        self.playAgainButton.addTarget(self, action: "playAgainButtonAction:", forControlEvents: UIControlEvents.TouchUpInside)
        delay(0.5) {
            view.addSubview(self.playAgainButton)
        }
        
        //label to hold the plays current score
        self.scoreLabel = UILabel(frame: CGRectMake(self.frame.size.width / 2, 200, 120, 120 ))
        self.scoreLabel.textAlignment = NSTextAlignment.Center
        self.scoreLabel.textColor = textGreenColour
        self.scoreLabel.text = "\(kScore)"
        self.scoreLabel.font = UIFont(name: scoreLabel.font.fontName, size: 45)
        self.scoreLabel.layer.anchorPoint = CGPointMake(1.0, 1.0)
        self.scoreLabel.layer.zPosition = 1
        
        //set back ground image for the score label
        self.scoreLabelImageView = UIImageView(image: scoreLabelImage!)
        self.scoreLabelImageView.frame = CGRectMake(self.frame.size.width / 2, 200, 120, 120)
        self.scoreLabelImageView.layer.anchorPoint = CGPointMake(1.0, 1.0)
        self.scoreLabelImageView.layer.zPosition = 0
        
        delay(0.5){
            //play audio
            self.playGameOverSceneAudio()
        }
        
        delay(0.5){
            view.addSubview(self.scoreLabel)
            view.addSubview(self.scoreLabelImageView)
        }
        
        //retrive mutestate from NS user defults
        /*kMuteState = userSettingDefults.boolForKey("MuteState")
        
        
        //set up mute and Unmute button
        self.muteButton = UIButton(type: UIButtonType.Custom)
        self.muteButton.frame = CGRectMake(self.frame.size.width / 4, 150, 80, 80)
        self.muteButton.layer.anchorPoint = CGPointMake(1.0, 1.0)
        self.muteButton.layer.zPosition = 0
        self.muteButton.addTarget(self, action: "muteButtonAction", forControlEvents: UIControlEvents.TouchUpInside)
        
        //add to subview with delay
        
        delay(0.5) {
            //add to sub view
            view.addSubview(self.muteButton)
        }
        
        // chech to see if retrived mute state is either true/false then display pic
        if kMuteState == false {
            self.muteButton.setImage(audioUnmutedImage, forState: .Normal)
        } else if kMuteState == true {
            self.muteButton.setImage(audioMutedButtomImage, forState: .Normal)
        }*/
        
        
        // create a face book button
        self.fbButton = UIButton(type: UIButtonType.Custom)
        self.fbButton.setImage(fbImage, forState: .Normal)
        self.fbButton.frame = CGRectMake(self.frame.size.width /  2, 450, 80, 80)
        self.fbButton.layer.anchorPoint = CGPointMake(1.0, 1.0)
        self.fbButton.layer.zPosition = 0
        // make a button perform an action
        self.fbButton.addTarget(self, action: "fbButtonAction", forControlEvents: UIControlEvents.TouchUpOutside)
        // add to sub view with delay
        delay(0.5){
            view.addSubview(self.fbButton)
        }
        
    }
    
    func delay(delay: Double, closure: ()->()) {
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC))),dispatch_get_main_queue(), closure)
    }
    
    
    func playAgainButtonAction(sender: UIButton) {
        
        delay(0.5) {
            //play the game again
            self.playAgain()
        }
        
        //stop the game audio from playing
        gameOverAudio.stop()
    }
    
    func playGameOverSceneAudio(){
        
        //setting up the audio for the game over scene
        do {
            try gameOverAudio = AVAudioPlayer(contentsOfURL: gameOverSoundEffect)
        } catch {
                print("GameScene. gameSceneAudioPlayer is not available")
        }
        gameOverAudio.prepareToPlay()
        gameOverAudio.numberOfLoops = -1
        
        //check to see if mute is true/false
        if kMuteState == true {
            //sound needs to be muted
            if (gameOverAudio.playing) {
            gameOverAudio.stop()
            }
        } else if kMuteState == false {
            // the sound can be played
            gameOverAudio.play()
        }
        
    }

    
    /*func muteButtonAction(sender: UIButton!) {
        
        if kMuteState == false {
            //the audio is unmuted
            userSettingDefults.setBool(true, forKey: "MuteState")
            userSettingDefults.synchronize()
            gameOverAudio.stop()
            self.muteButton.setImage(audioMutedButtomImage, forState: .Normal)
            kMuteState = userSettingDefults.boolForKey("MuteState")
            
        } else if kMuteState == true {
            //audio is muted
            userSettingDefults.setBool(false, forKey: "MuteState")
            userSettingDefults.synchronize()
            gameOverAudio.play()
            self.muteButton.setImage(audioUnmutedImage, forState: .Normal)
            kMuteState = userSettingDefults.boolForKey("MuteState")
            
        }
        
        
    }*/
    
    func fbButtonAction(sender: UIButton!){
        
        //call the notifcation that holds the selector
        NSNotificationCenter.defaultCenter().postNotificationName("FaceBookID", object: nil)
        
    }
    
    func playAgain() {
        
        //removes the score label from the view
        scoreLabel.removeFromSuperview()
        
        //removes the score label image View from the view
        scoreLabelImageView.removeFromSuperview()
        
        //removes the play agin button from view
        playAgainButton.removeFromSuperview()
        
        //remove the mute/Unmute button
        muteButton.removeFromSuperview()
        
        //removes the fb button from view
        fbButton.removeFromSuperview()
        
        
        //create the new scene to transition to
        let SkView = self.view! as SKView
        SkView.ignoresSiblingOrder = true
        // remove scene before transion
        self.scene?.removeFromParent()
        //the transion effect
        let transion = SKTransition.fadeWithColor(UIColor.grayColor(), duration: 1.0)
        transion.pausesOutgoingScene = false
        //a varible to hold the gameOver scene
        var scene: PlayGameScene!
        scene = PlayGameScene(size: SkView.bounds.size)
        //setting the new scene's aspect ration to fit
        scene.scaleMode = .AspectFill
        //presenting the new scene with a transition affect
        SkView.presentScene(scene, transition: transion)
        
    }
    
    
    
}