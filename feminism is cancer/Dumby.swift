//
//  Bumby.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright © 2016 Dixon, Matthew Stanley. All rights reserved.
//

import Foundation
import SpriteKit

class Dumby : SKSpriteNode {
    
    let DumbyTexture = SKTexture(imageNamed: "Birdy1")
    
    init(size: CGSize){
        super.init(texture: DumbyTexture, color: UIColor.clearColor(), size: CGSizeMake(size.width, size.height))
        zPosition = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}