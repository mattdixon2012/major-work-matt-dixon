
//
//  File.swift
//  feminism is cancer
//
//  Created by Dixon, Matthew Stanley on 16/08/2016.
//  Copyright © 2016 Dixon, Matthew Stanley. All rights reserved.
//

import Foundation
import SpriteKit

class Birdy : SKSpriteNode {
    
    let MovingBirdy1Texture = SKTexture(imageNamed: "Birdy1")
    let MovingBirdy2Texture = SKTexture(imageNamed: "Birdy2")
    
    init(size: CGSize) {
        super.init(texture: MovingBirdy1Texture, color: UIColor.clearColor(), size: CGSizeMake(size.width, size.height))
        zPosition = 1
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func Begin() {
        // fappy wings animation
        
        let animation = SKAction.animateWithTextures([MovingBirdy1Texture, MovingBirdy2Texture], timePerFrame: 0.2)
        
        runAction(SKAction.repeatActionForever(animation))
        
    }
}